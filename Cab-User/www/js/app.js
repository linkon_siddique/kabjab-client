var cabApp= angular.module('cab-app',['ngTouch','authController', 'services','ui.router']);
// booking-page-1.html BookingController
cabApp.config(function($stateProvider, $urlRouterProvider) {
  $urlRouterProvider.otherwise('/')

  $stateProvider.state('booking', {
    url: '/',
     templateUrl: 'view/booking-page-1.html',
    controller: 'BookingController'
  })

  $stateProvider.state('login', {
    url: '/login',
    templateUrl: 'view/login.html',
    controller: 'LoginController'
  })

  $stateProvider.state('register', {
    url: '/register',
    templateUrl: 'view/register.html',
    controller: 'RegisterController'
  })
  
  $stateProvider.state('booking_page_2', {
    url: '/booking-page-2',
    templateUrl: 'view/booking-page-2.html',
    controller: 'BookingController' 
  })
  
  $stateProvider.state('car_detail',{
    url: '/car-detail',
    templateUrl: 'view/car-detail.html',
    controller: 'CarDetailController'
  })
  
  $stateProvider.state('payment',{
    url: '/payment',
    templateUrl: 'view/payment-page.html',
    controller: 'PaymentController'
  })
  
  $stateProvider.state('car_booking',{
    url: '/car-booking',
    templateUrl: 'view/car-booking.html',
    controller: 'CarBookingController'
  })
  
  $stateProvider.state('driver_location',{
    url: '/driver-location',
    templateUrl: 'view/driver-location.html',
    controller: 'FormTitleController'
  })
  
    $stateProvider.state('rating',{
    url: '/rating',
    templateUrl: 'view/driver-rating.html',
    controller: 'RatingController'
  })
    
    $stateProvider.state('job_list',{
    url: '/job-list',
    templateUrl: 'view/job-list.html',
    controller: 'JobListController'
  })
});

/*
cabApp.directive('modalDialog', function() {
  return {
    restrict: 'E',
    scope: {
      show: '='
    },
    replace: true, // Replace with the template below
    transclude: true, // we want to insert custom content inside the directive
    link: function(scope, element, attrs) {
      scope.dialogStyle = {};
      if (attrs.width)
        scope.dialogStyle.width = attrs.width;
      if (attrs.height)
        scope.dialogStyle.height = attrs.height;
      scope.hideModal = function() {
        scope.show = false;
      };
    },
    templateUrl: 'modal-box.html' // See below
  };
});
*/

