var authController= angular.module('authController', ['firebase']);

authController.controller('AuthBtnController',['$scope', '$location',function($scope, $location){
$scope.registerPage= function(){
$location.path('/register');
};
    $scope.loginPage= function(){
    
    $location.path('/login');
    };

}]);
/** json url: http://cirestserver.agilecrafts.com/index.php/api/restacraft/users/format/json **/

authController.controller('LoginController',['$scope','$location','AuthService', function($scope,  $location, AuthService){
    
     $scope.email= '';
    $scope.password= '';
$scope.registerPage = function(){
    
     $location.path('/register');
    };    
    
$scope.login= function(){
    
    $scope.loginURLParams= {
        email: $scope.email, 
        password: $scope.password
    };
    //alert($scope.urlParams.email);
    //alert($scope.urlParams.password);
    //alert($scope.loginURLParams.email);
    AuthService.login($scope.loginURLParams);

};
    
}]);

authController.controller('CarBookingController',['$scope','$location','MapService','BookingService', function($scope, $location, MapService, BookingService){
    $scope.fromLocation= MapService.fromLocation;
    $scope.toLocation = MapService.toLocation;
    $scope.viaLocation= MapService.viaLocation;  
    $scope.submitBooking= function(){
    
        BookingService.bookingReqSubmit();
       // $location.path('/job-list');
        
    };
}]);

authController.controller('JobListController',['$scope','$location','BookingService','$interval','MapService','$firebaseObject', function($scope, $location, BookingService, $interval, MapService, $firebaseObject){
    //alert('job list');

    var ref = new Firebase("https://driver-track.firebaseio.com/");
    //alert('ref worked');
    var listen = $firebaseObject(ref);
    //alert('sync worked');
    
    //var listen = $firebaseArray(ref);

    //$scope.data.$bindTo($scope, "pos");
//    listen.$loaded().then(function() {
//    //alert('changed');
//        //alert('the lat: '+ listen.latitude);
//  })
//  .catch(function(err) {
//    console.error(err);
//  });
    
    //$scope.driverLat= 
    
    var latLongArr= [];
    var tempLat, tempLong, driverPosChecker;
    BookingService.acceptedItem('u@d.com');
    MapService.watchCurrentPosition().then(function(position){
        MapService.updateCurrentLat(position.coords.latitude);
        MapService.updateCurrentLong(position.coords.longitude);
        listen.$loaded().then(function(){
        latLongArr=[
        [MapService.currentLatitude, MapService.currentLongitude],
        [listen.latitude, listen.longitude]  
        ];    
        tempLat= listen.latitude;
        tempLong= listen.longitude;    
        MapService.getMap(listen.latitude, listen.longitude);   
        //alert(MapService.latlng);    
        MapService.drawMap(MapService.latlng, 'live_track', 17).then(function(){
        //alert(MapService.latlng);    
        MapService.addMultipleMarker(latLongArr).then(function(latlongArr){
           //MapService.addLineBtwnMarkers(latlongArr);
           listen.$watch(function(event) {
            MapService.updateMarkerPos(listen.latitude, listen.longitude);
        //alert(listen.latitude);
});
             
        }, function(){});
            
        }, function(){});
       
    }).catch(function(err) {
    //console.error(err);
  });
        
    }, function(){});
    

//    driverPosChecker= $interval(function(){
//    BookingService.getAssignedDriverLatLong('u@d.com').then(function(){
//        if(BookingService.currentJob[0].assign_driver_lat==tempLat && BookingService.currentJob[0].assign_driver_long== tempLong){
//            return;    
//        }
//        else{
//        
//        MapService.updateMarkerPos(MapService.markerArray[1], BookingService.currentJob[0].assign_driver_lat, BookingService.currentJob[0].assign_driver_long);
//        
//        }
//        
//    }, function(){});
//    
//    }, 60000);
    $scope.acceptedJobs= BookingService.acceptedJobs;
    $scope.$watch(function() {
       return BookingService.acceptedJobs;
     },                       
      function() {
        $scope.acceptedJobs= BookingService.acceptedJobs;
    }, true);
    var timer = $interval(function(){
            BookingService.acceptedItem('u@d.com');
        },120000);
    
    
}]);

authController.controller('RegisterController',['$scope','$location','AuthService', function($scope,  $location, AuthService){
    $scope.bookingPage = function(){
    //location.href='index.html';
     $location.path('/');
    //$location.path('/');
    };  
    $scope.loginPage= function(){
    
        $location.path('/login');
    };

    var url= 'http://cirestserver.agilecrafts.com/index.php/api/restacraft/registerUser';

    $scope.register= function(){
 $scope.registerURLParams= {
     name: $scope.name, 
     password: $scope.password, 
     email: $scope.email, 
     mobile: $scope.mobile
    };
AuthService.register($scope.registerURLParams);
    };
    
}]);


authController.controller('BookingController',['$scope','$http','$location','MapService','BookingService', function($scope, $http, $location, MapService, BookingService){

    var selectedTextBox=null;
    var drawCounter=0;
    var fromLat,watchID,fromLong, toLat, toLong, viaLat, viaLong, isPositionFound=false, tempLat=null, tempLong=null;
     
    var watchCurrentPosition= function(){
    //var watchPosDefer= $q.defer();
    var options = {timeout:1000};
    var geoCounter=0;
    if(navigator.geolocation){
        watchID=  navigator.geolocation.watchPosition(function (position) {
                         if(tempLat!=position.coords.latitude || tempLong!=position.coords.longitude){
                //alert("changed");
                tempLat=position.coords.latitude;
                tempLong=position.coords.longitude;
        MapService.updateCurrentLat(position.coords.latitude);
        MapService.updateCurrentLong(position.coords.longitude);
        MapService.getMap(MapService.currentLatitude, MapService.currentLongitude);
        MapService.convertLatLongToPlace(MapService.latlng).then(function(){
           $scope.currLocation= MapService.currentLocation;
            isPositionFound= true;
            $scope.apply();
            
        }, function(){}); 
        
                if(++geoCounter==1){
                MapService.inputFieldMapSearch('from');
                MapService.inputFieldMapSearch('to');
                MapService.inputFieldMapSearch('via');
            }   
                    //watchPosDefer.resolve(position);
            }
            }, function(error) {
                alert("geo error");
               
            });
            
    
        /*watchID= intel.xdk.geolocation.watchPosition(function(position) {
            //alert('changed');
            //alert(position.coords.latitude);
            if(tempLat!=position.coords.latitude || tempLong!=position.coords.longitude){
                //alert("changed");
                tempLat=position.coords.latitude;
                tempLong=position.coords.longitude;
        MapService.updateCurrentLat(position.coords.latitude);
        MapService.updateCurrentLong(position.coords.longitude);
        MapService.getMap(MapService.currentLatitude, MapService.currentLongitude);
        MapService.convertLatLongToPlace(MapService.latlng).then(function(){
           $scope.currLocation= MapService.currentLocation;
            isPositionFound= true;
            $scope.apply();
            
        }, function(){}); 
        
                if(++geoCounter==1){
                MapService.inputFieldMapSearch('from');
                MapService.inputFieldMapSearch('to');
                MapService.inputFieldMapSearch('via');
            }   
                    //watchPosDefer.resolve(position);
            }
            }, function(error) {
                alert("geo error");
               
            });
*/        
    }
    else {
        alert("Geolocation error");

}

    //return watchPosDefer.promise;
};

        watchCurrentPosition();
    $scope.showMap= function(textBox){
        if(isPositionFound){
            drawCounter++;
        }
        
        selectedTextBox= textBox;
        document.getElementById('modal').style.visibility='visible';
        document.getElementById('modal').style.opacity='1';
        document.getElementById('modal').style.top='50%';
        if(drawCounter==1){
            MapService.drawMap(MapService.latlng, "map_canvas", 17).then(function(){
            MapService.addMarker(MapService.latlng, $scope.currLocation);
            MapService.addInfoWindow($scope.currLocation);      
            google.maps.event.addListener(MapService.map,'click', function(e) {
       //alert('clicked @'+e.latLng.toString());
        MapService.convertToPlace(e.latLng).then(function(currSelPlace){
            document.getElementById(selectedTextBox).value= currSelPlace;
            switch(selectedTextBox){
                
                        case 'from':
                            fromLat= MapService.currentLatitude;
                            fromLong= MapService.currentLongitude;
                        //alert(fromLat);
                            break;
                        case 'to':
                            toLat= MapService.currentLatitude;
                            toLong= MapService.currentLongitude;
                            break;
                        case 'via':
                            viaLat= MapService.currentLatitude;
                            viaLong= MapService.currentLongitude;
                            break;
                        default:
                            break;
                
                }
        }, function(){ });
          
      });
            
            },function(){ });
        }
        
    };
    
    $scope.hideMap= function(){
    
        document.getElementById('modal').style.visibility='hidden';
        document.getElementById('modal').style.opacity='0';
    
    };
    
    $scope.user= true;
    $scope.val= true;
    $scope.toggleDatePicker= function(){

        $scope.val= $scope.user;
    };
    
    function ISODateString(d){
 function pad(n){return n<10 ? '0'+n : n}
 return d.getUTCFullYear()+'-'
      + pad(d.getUTCMonth()+1)+'-'
      + pad(d.getUTCDate())+'T'
      + pad(d.getUTCHours())+':'
      + pad(d.getUTCMinutes())+':'
      + pad(d.getUTCSeconds())+'Z'}

    $scope.bookPage2= function(){
        if($scope.datePicker){
            BookingService.updateDate(ISODateString($scope.datePicker));
        }
        $scope.currLocation=document.getElementById('from').value;
        $scope.toLocation=document.getElementById('to').value;
        $scope.viaLocation=document.getElementById('via').value;
        MapService.updateFromLocation($scope.currLocation, fromLat, fromLong);
        MapService.updateToLocation($scope.toLocation, toLat, toLong);
        MapService.updateViaLocation($scope.viaLocation, viaLat, viaLong);
        $location.path('/register');
    };
 
    $scope.bookpage1= function(){
        $location.path('/');
    };
    $scope.registerPage= function(){
        $location.path('/register');
    };
}]);      


authController.controller('CarDetailController',['$scope', '$location',function($scope, $location){
    $scope.cheapest= false;
    $scope.quickest= $scope.expensive= $scope.ratingOrder= true;
    
    $scope.showCheapest= function(){
        $scope.cheapest= false;
        $scope.quickest= $scope.expensive= $scope.ratingOrder= true;
    };
    $scope.showQuickest= function(){
        $scope.quickest= false;
        $scope.cheapest= $scope.expensive= $scope.ratingOrder= true;
    };
      $scope.showExpensive= function(){
        $scope.expensive= false;
        $scope.cheapest= $scope.quickest= $scope.ratingOrder= true;
    };
       $scope.showRatingOrder= function(){
        $scope.ratingOrder= false;
        $scope.cheapest= $scope.quickest= $scope.expensive= true;
    };
    $scope.logout= function(){
        $location.path('/login');
    
    };
}]);  

authController.controller('PaymentController',['$scope', '$location',function($scope, $location){
    //alert('payment');
    $scope.detailPage= function(){
    
        $location.path('/car-detail');
    };
    
    $scope.formTitlePage= function(){
   // alert('titlepage');
    $location.path('/driver-location');
    
    };
}]);  
//driver location controller
authController.controller('FormTitleController',['$scope', '$location',function($scope, $location){

}]); 



authController.controller('RatingController',['$scope', '$location',function($scope, $location){
     $scope.titlePage= function(){
    
    $location.path('/driver-location');
    
    };
    //alert('Detail Car');
}]);  


