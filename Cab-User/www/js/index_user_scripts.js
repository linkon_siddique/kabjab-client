(function()
{
 "use strict";
 /*
   hook up event handlers 
 */
 function register_event_handlers()
 {
    
    
     /* button  Login */
    $(document).on("click", ".uib_w_1", function(evt)
    {
         //activate_page("#login-page"); 
    });
    
        /* button  Register */
    $(document).on("click", ".uib_w_2", function(evt)
    {
         //activate_page("#register-page"); 
    });
    
        /* button  .uib_w_9 */
    $(document).on("click", ".uib_w_9", function(evt)
    {
         activate_page("#mainpage"); 
    });
    
        /* button  .uib_w_10 */
    $(document).on("click", ".uib_w_10", function(evt)
    {
         activate_page("#mainpage"); 
    });
    
        /* button  .uib_w_19 */
    $(document).on("click", ".uib_w_19", function(evt)
    {
         //activate_page("#booking-page-2"); 
    });
    
        /* button  Cheapest */
    $(document).on("click", ".uib_w_35", function(evt)
    {
         //activate_subpage("#cheapest-page"); 
    });
    
        /* button  Quickest */
    $(document).on("click", ".uib_w_36", function(evt)
    {
        //alert('quickest');
         //activate_subpage("#quickest-page"); 
    });
    
        /* button  Expensive */
    $(document).on("click", ".uib_w_37", function(evt)
    {
         //activate_subpage("#expensive-page"); 
    });
    
        /* button  By Rating */
    $(document).on("click", ".uib_w_38", function(evt)
    {
         //activate_subpage("#rating-order-page"); 
    });
    
        /* button  .uib_w_65 */
    $(document).on("click", ".uib_w_65", function(evt)
    {
         //activate_page("#cab-details-page"); 
    });
    
     
        /* button  .uib_w_95 */
    $(document).on("click", ".uib_w_95", function(evt)
    {
        /* your code goes here */ 
        //$('.widget uib_w_96').show();
    });
    
    }
 document.addEventListener("app.Ready", register_event_handlers, false);
})();
