var appService= angular.module('services',[]); 

appService.factory('AuthService',['$location','$http','$rootScope', function($location, $http, $rootScope){

return{
    
    email: '',
    
    login: function(urlParam){
      var  url= 'http://cirestserver.agilecrafts.com/index.php/api/restacraft/login';
        $http.get(url, {params:{email: urlParam.email, pwd: urlParam.password
        }}).success(function (data){
       
            if(!data.error){
        //AuthService.email= urlParam.email;
        $location.path('/car-booking');
    }
    else
        alert('invalid data: '+data.error);
         
     }).error(function(err) {
    //alert('error: ' + err);
});
    
    },
    
    register: function(urlParams){
    var registerURL= 'http://cirestserver.agilecrafts.com/index.php/api/restacraft/registerUser';
        $http.post(registerURL,{name: urlParams.name, password: urlParams.password, email: urlParams.email, mobile: urlParams.mobile}).success(function (data){
    //alert("succeded"+$scope.password);
            $location.path('/car-booking');
     }).error(function(err) {
    //alert('error' + err);
});
        
    }
};

}]);


appService.factory('MapService',['$location','$http','$rootScope','$q',function($location, $http, $rootScope, $q){

var MapData={
    map: null,
    infoWindow: null,
    marker: null,
    markerArray:[],
    fromLocation: null,
    toLocation: null,
    viaLocation: null,
    fromLatitude: null,
    fromLongitude: null,
    toLatitude: null,
    toLongitude: null,
    viaLatitude: null,
    viaLongitude: null,
    latlng: null,
    geocoder: null,
    currentLatitude: null,
    currentLongitude: null,
    currentLocation: null,
    currentSelectedPlace: null,
    latlngbounds: null,
    watchID: null
};


MapData.updateFromLocation= function(value, lat, long){
    
        this.fromLocation= value;
        this.fromLatitude= lat;
        this.fromLongitude= long;
        $rootScope.$broadcast('currLocUpdated');
        //alert(this.fromLocation);
    
    };
    

MapData.updateToLocation= function(value, lat, long){
    
        this.toLocation= value;
        this.toLatitude= lat;
        this.toLongitude= long;
        $rootScope.$broadcast('toLocUpdated');    
        //alert(this.toLocation);
    };
MapData.updateViaLocation= function(value, lat, long){
    
        this.viaLocation= value;
        this.viaLatitude= lat;
        this.viaLongitude=long;
        $rootScope.$broadcast('viaLocUpdated');  
        //alert(this.viaLocation);
    
    };
    
MapData.getMap= function(lat, long) {
    //Creates a new google maps object
        this.latlng = new google.maps.LatLng(lat,long);

};

MapData.getLatLong= function(lat, long){
    return new google.maps.LatLng(lat,long);
};    
        
MapData.updateCurrentLat= function(currLat){
    this.currentLatitude= currLat;
};
    
MapData.updateCurrentLong= function(currLong){
    this.currentLongitude= currLong;
};
    
MapData.getCurrentLocation= function(){
    
var currPositionDeferred= $q.defer();
    if(navigator.geolocation){
        //alert('get postion');
           // use this chunk of code when test in real device
         navigator.geolocation.getCurrentPosition(function (position) {
                $rootScope.$apply(function() {
                    currPositionDeferred.resolve(position);
                });
            }, function (error) {
                $rootScope.$apply(function() {
                    currPositionDeferred.reject(error);
                   MapData.getCurrentLocation(); 
                });
            });
     
           // use this chunk of code when test in emulator
       /* intel.xdk.geolocation.getCurrentPosition(function(position) {
                $rootScope.$apply(function() {
                    currPositionDeferred.resolve(position);
                });
            }, function(error) {
                $rootScope.$apply(function() {
                    currPositionDeferred.reject(error);
                });
            });*/
        }
    else {
        $rootScope.$apply(function() {
                currPositionDeferred.reject(new Error("Geolocation is not supported"));
            });

}
    
    return currPositionDeferred.promise;
};

MapData.convertLatLongToPlace= function(latlong) {

    var latLongConvertDeferred= $q.defer();
    this.geocoder = new google.maps.Geocoder();
    this.geocoder.geocode({'latLng': latlong}, function(results, status) {
      if (status == google.maps.GeocoderStatus.OK) {
      //console.log(results);
        if (results[1]) {
         //formatted address
         //alert(results[0].formatted_address)
        //find country name
             for (var i=0; i<results[0].address_components.length; i++) {
            for (var b=0;b<results[0].address_components[i].types.length;b++) {

            //there are different types that might hold a city admin_area_lvl_1 usually does in come cases looking for sublocality type will be more appropriate
                if (results[0].address_components[i].types[b] == "administrative_area_level_1") {
                    //this is the object you are looking for
                    city= results[0].address_components[i];
                    break;
                }
            }
        }
        //city data
            //var cityName=city.short_name;
         //alert(city.short_name + " " + city.long_name);
            $rootScope.$apply(function(){
                MapData.currentLocation = results[0].formatted_address;
            });
            latLongConvertDeferred.resolve();
            
           //alert('current loc'+this.currentLocation);
            //document.getElementById('pac-input').value = $scope.currLocation;
            //alert('current location: '+ currLocation);
         //alert($scope.currLocation);
        } else {
          alert("No results found");
        }
      } else {
          alert("Geocoder failed due to: " + status);
          latLongConvertDeferred.reject();
        
      }
         
    });
    return latLongConvertDeferred.promise;
  }; 
    
    
MapData.inputFieldMapSearch= function(inputFieldID){
    
        var inputField = document.getElementById(inputFieldID);
        var autocompleteSearch= new google.maps.places.Autocomplete((inputField),
                {
                    types: ['geocode'],
                });
        google.maps.event.addListener(autocompleteSearch, 'place_changed', function() {
            var placeSearch = autocompleteSearch.getPlace();
            if (!placeSearch.geometry) {
                return;
            }
            //document.getElementById('fLat').value = place.geometry.location.lat();
            //document.getElementById('fLong').value = place.geometry.location.lng();

            var addresses = '';
            if (placeSearch.address_components)
            {
                addresses = [
                    (placeSearch.address_components[0] && placeSearch.address_components[0].short_name || ''),
                    (placeSearch.address_components[1] && placeSearch.address_components[1].short_name || ''),
                    (placeSearch.address_components[2] && placeSearch.address_components[2].short_name || '')
                ].join(' ');
            }
        });
    };

MapData.addMarker= function(latlng, markerTitle){
    
    this.marker = new google.maps.Marker({
        position: latlng,
        title: markerTitle,
          
});
// To add the marker to the map, call setMap();
    this.marker.setMap(this.map);
    this.markerArray.push(this.marker);
}; 

MapData.updateMarkerPos= function(lat, long){
var pos= new google.maps.LatLng(lat, long);
    
    MapData.markerArray[1].setPosition(pos);
    this.map.panTo(pos);
    //alert('yooooo');
    /*var icon='images/car-icon-01.png';
    var markerIcon= new google.maps.MarkerImage(icon);
    markerInstance = new google.maps.Marker({
        position: pos,
        map: map,
        icon: markerIcon
});*/
    //alert(MapData.markerArray[0].getPosition());
    
        //alert('inside for loop');
    this.latlngbounds.extend(MapData.markerArray[0].getPosition());
    this.latlngbounds.extend(MapData.markerArray[1].getPosition());
        
    
    
    this.map.fitBounds(this.latlngbounds);
};    
    
MapData.closeAndResetMarker= function(latlng, markerTitle){
        for (var i = 0, marker; marker = this.markerArray[i]; i++) {
              marker.setMap(null);
            }
        this.markerArray = [];        
        this.addMarker(latlng, markerTitle);
              
    
    };    
    
MapData.addInfoWindow= function(infoWindowContent){

    this.infoWindow = new google.maps.InfoWindow({
        content: infoWindowContent
  });
    this.infoWindow.open(this.marker.get('map'), this.marker);      
};    

MapData.setMapInstance= function(mapID, mapOptions){

    this.map = new google.maps.Map(document.getElementById(mapID), mapOptions);

};    
    
MapData.drawMap= function(latlng, mapID, zoomLevel){
    
    var mapDrawDeferred= $q.defer();
    var mapOptions = {
        center: latlng,
        zoom: zoomLevel,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        zoomControl: true,
        zoomControlOptions: {
            style: google.maps.ZoomControlStyle.SMALL,
            position: google.maps.ControlPosition.LEFT_TOP
        }
    };
    this.setMapInstance(mapID, mapOptions);
    var remove_poi = [
    {
      "featureType": "poi",
      "elementType": "labels",
      "stylers": [
        { "visibility": "off" }
      ]
    }
  ];
    this.map.setOptions({styles: remove_poi});   
    mapDrawDeferred.resolve();
    return mapDrawDeferred.promise;
 };

MapData.addMultipleMarker= function(locations){
     var markerDeferred= $q.defer();
     this.latlngbounds = new google.maps.LatLngBounds();
     var latlongArr= [];
     var position= null;      
     var currLocMarker;
     var icon='images/car-icon-01.png';
     var infoWindow = new google.maps.InfoWindow();
     var markerIcon, markerAnimation;
     this.markerArray = []; 
     for (var i = 0; i < locations.length; i++) {  
         position= new google.maps.LatLng(locations[i][0], locations[i][1]);
         latlongArr.push(position);
         if(i==0){
             markerIcon='';
             //markerAnimation='';
         }
         else {
             markerIcon= new google.maps.MarkerImage(icon);
             //markerAnimation=google.maps.Animation.BOUNCE;
         }
       this.markerArray[i] = new google.maps.Marker({
    position: position,
          map: MapData.map,
           icon: markerIcon
});
         //this.markerArray.push(currLocMarker);
         this.latlngbounds.extend(this.markerArray[i].position);
        /* google.maps.event.addListener(this.markerArray[i], 'click', (function(currLocMarker, i) {
            return function() {
                infoWindow.setContent(locations[i][0]);
                infoWindow.open(map, this.markerArray[i]);
            }
        })(this.markerArray[i], i));*/
         
    
     }
     
    this.map.fitBounds(this.latlngbounds);
    markerDeferred.resolve(latlongArr);
    return markerDeferred.promise;
      //markers.push(currLocMarker);

};    

MapData.addLineBtwnMarkers= function(latlongArr){
        //Initialize the Path Array
        var path = new google.maps.MVCArray();
    
        //Initialize the Direction Service
        var service = new google.maps.DirectionsService();
 
        //Set the Path Stroke Color
        var poly = new google.maps.Polyline({ map: MapData.map, strokeColor: '#FF0000' });
 for (var i = 0; i < latlongArr.length; i++) {
            if ((i + 1) < latlongArr.length) {
                var src = latlongArr[i];
                var des = latlongArr[i + 1];
                path.push(src);
                poly.setPath(path);
                service.route({
                    origin: src,
                    destination: des,
                    travelMode: google.maps.DirectionsTravelMode.DRIVING
                }, function (result, status) {
                    if (status == google.maps.DirectionsStatus.OK) {
                        for (var i = 0, len = result.routes[0].overview_path.length; i < len; i++) {
                            path.push(result.routes[0].overview_path[i]);
                        }
                    }
                });
            }
        }

};     
    
MapData.convertToPlace= function(latlng){
            var placeSelDeferred= $q.defer();
            this.setPlaceName(latlng,function(converted){
               MapData.currentSelectedPlace= converted;
               MapData.closeMarkerInfoWindow();
               MapData.closeAndResetMarker(latlng, MapData.currentSelectedPlace); 
               MapData.addInfoWindow(MapData.currentSelectedPlace);
               placeSelDeferred.resolve(MapData.currentSelectedPlace);
            //infowindow.close();
           
    //infowindow.setContent('<div><strong>' + converted);
    //infowindow.open(newMarker.get('map'), newMarker);
            });      
    return placeSelDeferred.promise;
    }; 
    
MapData.setPlaceName= function(latlong, callback){
        var placeResult;
        this.geocoder.geocode({'latLng': latlong}, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
      //console.log(results);
                if (results[1]) {
         //formatted address
         //alert(results[0].formatted_address)
        //find country name
           
                for (var i=0; i<results[0].address_components.length; i++) {
                for (var b=0;b<results[0].address_components[i].types.length;b++) {

            //there are different types that might hold a city admin_area_lvl_1 usually does in come cases looking for sublocality type will be more appropriate
                    if (results[0].address_components[i].types[b] == "administrative_area_level_1") {
                    //this is the object you are looking for
                    city= results[0].address_components[i];
                    break;
                }
            }
        }
        placeResult= results[0].formatted_address;            
        MapData.updateCurrentLat(parseFloat(results[0].geometry.location.lat()));
        MapData.updateCurrentLong(parseFloat(results[0].geometry.location.lng()));
            callback(placeResult);
        } else {
          alert("No results found");
        }
      } else {
        alert("Geocoder failed due to: " + status);
      }
         
    });
        
    };
    
MapData.closeMarkerInfoWindow= function(){

    this.infoWindow.close();

};    
       
    
    return MapData;

}]);


appService.factory('BookingService',['$location','$http','$rootScope','MapService','$q',function($location, $http, $rootScope, MapService, $q){

    var Booking= {
            dateTime:null,
            acceptedJobs: null,
            currentJob: null
    };
    Booking.updateDate= function(dateTime){
    
        this.dateTime= dateTime;
        //alert(this.dateTime);
    };
    Booking.bookingReqSubmit= function(){
        var bookingURL= 'http://cirestserver.agilecrafts.com/index.php/api/restacraft/requisition';
            $http.post(bookingURL,{from_location: MapService.fromLocation, to_location: MapService.toLocation, via_location: MapService.viaLocation,  from_latitude: MapService.fromLatitude, to_latitude: MapService.toLatitude, via_latitude: MapService.viaLatitude, from_longitude: MapService.fromLongitude, to_longitude: MapService.toLongitude, via_longitude: MapService.viaLongitude, date_time: this.dateTime }).success(function (data){

        //alert("succeded req");

        $location.path('/job-list');

         }).error(function(err) {
        //alert('error' + err);
    });

    };
    
    Booking.acceptedItem= function(driverEmail){
        var acceptedJobURL= 'http://cirestserver.agilecrafts.com/index.php/api/restacraft/getAcceptedRequisition';

        $http.get(acceptedJobURL, {params:{driveremail: driverEmail
        }}).success(function (data){
            //alert("success");
            if(!data.error){
                if($rootScope.$root.$$phase != '$apply' && $rootScope.$root.$$phase != '$digest'){
   $rootScope.$apply(function() {
            Booking.acceptedJobs= data;
   });
 }
                else Booking.acceptedJobs= data;
                     
            }
            else{
                //alert('invalid data: '+data.error);
            }
         }).error(function(err) {
            //alert('error: ' + err);
        });
    
    };
    
    Booking.getAssignedDriverLatLong= function(driverEmail){
    var driverLatLongdeferred = $q.defer();
    var currentJobURL= 'http://cirestserver.agilecrafts.com/index.php/api/restacraft/getCurrentJob';

        $http.get(currentJobURL, {params:{driveremail: driverEmail
        }}).success(function (data){
            //alert("success");
            if(!data.error){
                if($rootScope.$root.$$phase != '$apply' && $rootScope.$root.$$phase != '$digest'){
   $rootScope.$apply(function() {
       //alert('data valid');
            Booking.currentJob= data;       
   });
 }
                else Booking.currentJob= data;
                   driverLatLongdeferred.resolve();  
            }
            else{
                alert('invalid data: '+data.error);
            }
         }).error(function(err) {
            driverLatLongdeferred.reject(err);
            //alert('error: ' + err);
        });
    return driverLatLongdeferred.promise;
    };
    
    return Booking;
    
}]);