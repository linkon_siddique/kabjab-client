/*angular.module('FoodApp', ['ionic'])
  
 .controller('AppCtrl', function() {

  ionic.Platform.ready(function() {
    navigator.splashscreen.hide();
  });

 });*/
              

var foodApp= angular.module('FoodApp',['ionic']);

foodApp.config(function($stateProvider, $urlRouterProvider) {
    
$urlRouterProvider.otherwise('/home')

  $stateProvider.state('home', {
    url: '/home',
    templateUrl: 'view/home.html',
    controller: 'HomeController'
  })

});