var foodApp= angular.module('food-app', ['ngRoute','foodListController']);
foodApp.config(['$routeProvider', function($routeProvider){

$routeProvider.
when('/list', {
    templateUrl: 'pages/review-list.html',
    controller: 'ReviewListController'
    
}).
when('/reviewdetails/:itemId',{

templateUrl: 'pages/review-details.html',
controller: 'ReviewDetailController'    
    
}).
otherwise({
 redirectTo: '/list'
    
});

}]);

/*when('reviewdetails/:itemId',{

templateUrl: 'pages/review-details.html',
controller: 'ReviewDetailController'    
    
})*/