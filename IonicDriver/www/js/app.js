// Code goes here

var app = angular.module('ionicApp', ['ionic', 'authController', 'services']);
//app.constant('FireBaseURL', 'https://driver-track.firebaseio.com/');
app.config(function($stateProvider, $urlRouterProvider) {
  $urlRouterProvider.otherwise('/login')

  $stateProvider.state('app', {
    url: '/login',
    templateUrl: 'view/login.html',
    controller: 'LoginController'
  })

  $stateProvider.state('home', {
    url: '/home',
    templateUrl: 'view/home.html',
    controller: 'HomeContorller'
  })

  $stateProvider.state('help', {
    url: '/help',
    templateUrl: 'view/help.html',
    controller: 'HelpController'
  })
  
  $stateProvider.state('detail', {
    url: '/detail/:fromLoc/:toLoc/:viaLoc/:fromLat/:fromLong/:toLat/:toLong/:viaLat/:viaLong',
    templateUrl: 'view/requisition-detail.html',
    controller: 'ReqDetailController' 
  })
  
});
