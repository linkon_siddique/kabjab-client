angular.module('ionicApp', ['ionic'])
  
 .controller('AppCtrl', function() {

  ionic.Platform.ready(function() {
    navigator.splashscreen.hide();
  });

 });

var cabApp= angular.module('cab-app',['ionic','ngRoute','ngTouch', 'authController']);              
cabApp.config(['$routeProvider', function($routeProvider){

    $routeProvider.
    when('/',{
    
        templateUrl: 'view/auth-button.html',
        controller: 'AuthBtnController'
    }).
    when('/login',{
    
        templateUrl: 'view/login.html',
        controller: 'LoginController'
    
    }).
    when('/register',{
    
        templateUrl: 'view/register.html',
        controller: 'RegisterController'
    
    }).
    when('/booking',{
        templateUrl: 'view/account-page.html',
        controller: 'BookingController'
    }).
    when('/booking-page-2',{
        templateUrl: 'view/booking-page-2.html',
        controller: 'BookingController'
    }).
    otherwise({
    
        redirectTo: '/'
    });
}]);