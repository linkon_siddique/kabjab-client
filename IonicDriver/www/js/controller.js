var authController= angular.module('authController', ['ui.router']);

authController.controller('AuthBtnController',['$scope', '$location',function($scope, $location){
    $scope.registerPage= function(){
        $location.path('/register');
    };
    $scope.loginPage= function(){
        $location.path('/login');
    };
}]);
/** json url: http://cirestserver.agilecrafts.com/index.php/api/restacraft/users/format/json **/

authController.controller('LoginController', function($scope, $location, $state, AuthService){
    $scope.home = function(){
         $location.path('/');
    };    
    $scope.login = function(){
        $scope.loginURLParams= {
            email: $scope.login.email, 
            password: $scope.login.password
        };
        AuthService.login($scope.loginURLParams, {
            success: function(){
                AuthService.updateDriverEmail($scope.loginURLParams.email);
                $state.transitionTo('home');
            },
            error: function(){}
        });
    };
    
});

authController.controller('ReqDetailController',['$scope','$location','$state','AuthService','$stateParams','MapService','DriverJobService', function($scope, $location, $state, AuthService, $stateParams, MapService, DriverJobService){

    $scope.fromLocation= $stateParams.fromLoc;
    $scope.toLocation= $stateParams.toLoc;
    $scope.viaLocation= $stateParams.viaLoc;
    var drawCounter=0;
    var locationArr=[
         [$scope.fromLocation, $stateParams.fromLat, $stateParams.fromLong],
         [$scope.toLocation, $stateParams.toLat, $stateParams.toLong]
     ];   
    
    MapService.getMap($stateParams.fromLat, $stateParams.fromLong);
    MapService.drawMap(MapService.latlng,'map_canvas',17).then(function(){
        MapService.addMultipleMarker(MapService.map, locationArr).then(function(latlongArr){
            MapService.addLineBtwnMarkers(MapService.map, latlongArr);
        }, function(){});
    }, function(){});
    
    $scope.backToHome = function(){
        $location.path('/home');
     }; 
}]);


authController.controller('RegisterController',['$scope','$http','$location', function($scope, $http, $location){
    $scope.home = function(){
    
    //location.href='index.html';
     $location.path('/');
    //$location.path('/');
    };  
$scope.register= function(){
     //$http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";
    var url= 'http://cirestserver.agilecrafts.com/index.php/api/restacraft/registerUser';
    //var encodedURL= encodeURIComponent(url);
$http.post(url,{name: $scope.name, password: $scope.password, email: $scope.email, mobile: $scope.mobile}).success(function (data){

    //alert("succeded"+$scope.password);
    $location.path('/');
  
     }).error(function(err) {
    alert('error' + err);
});

};
    
}]);
authController.controller('GymContorller',['$scope','$http', function($scope, $http){

alert('controllll');


}]);

authController.controller('HomeContorller',['$scope','$http','$location','DriverJobService','$interval','AuthService','MapService', function($scope, $http, $location, DriverJobService, $interval, AuthService, MapService){
    //alert('hi controller');
     $scope.assigned= false;
     $scope.accepted= false; 
     var checkModel=undefined;
    
/*    var driverPosTimer = $interval(function(){
     //MapService.getCurrentLocation().then(function(position){
        MapService.updateCurrentLat(position.coords.latitude);
        MapService.updateCurrentLong(position.coords.longitude);  
        //alert(MapService.currentLatitude, MapService.currentLongitude); 
        DriverJobService.updateDriverPosition(AuthService.driverEmail, MapService.currentLatitude, MapService.currentLongitude);
        
    }, function(){});
        },60000);
    
     
  MapService.getCurrentLocation().then(function(position){
        MapService.updateCurrentLat(position.coords.latitude);
        MapService.updateCurrentLong(position.coords.longitude);  
        //alert(MapService.currentLatitude, MapService.currentLongitude); 
        DriverJobService.updateDriverPosition(AuthService.driverEmail, MapService.currentLatitude, MapService.currentLongitude);
        
    }, function(){});*/
    
    MapService.watchCurrentPosition();
    
    $scope.toggleGroup = function(whichModel) {
    if(whichModel=='accepted'){
        $scope.accepted= !$scope.accepted; 
        $scope.assigned= false;
    }
        else{
            $scope.assigned= !$scope.assigned;
            $scope.accepted= false; 
        }
  };
    
    $scope.requisition= DriverJobService.requisitionData;
    $scope.acceptedList= DriverJobService.acceptedData;
    $scope.currentJobList= DriverJobService.currentJobData;
    DriverJobService.checkForCurrentJob(AuthService.driverEmail);
    DriverJobService.checkForNewJob(AuthService.driverEmail);
    DriverJobService.checkForAcceptedJob(AuthService.driverEmail);
    
    $scope.$watch(function() {
       return DriverJobService.currentJobData;
     },                       
      function() {
        $scope.currentJobList= DriverJobService.currentJobData;
    }, true);
    
    $scope.$watch(function() {
       return DriverJobService.requisitionData;
     },                       
      function() {
        $scope.requisition= DriverJobService.requisitionData;
    }, true);
    
    $scope.$watch(function() {
       return DriverJobService.acceptedData;
     },                       
      function() {
        $scope.acceptedList= DriverJobService.acceptedData;
    }, true);

    var timer = $interval(function(){
        //DriverJobService.checkForCurrentJob(AuthService.driverEmail);
        DriverJobService.checkForNewJob(AuthService.driverEmail);
        //DriverJobService.checkForAcceptedJob(AuthService.driverEmail);    
        },120000);

    $scope.accept= function(reqID, array, index){
        
        DriverJobService.reqAccept(reqID, AuthService.driverEmail, array, index);
        
    };
    $scope.hideMap= function(){

        document.getElementById('modal').style.visibility='hidden';
        document.getElementById('modal').style.opacity='0';
    };
    
    $scope.onTheWay= function(reqID, array, index){
        //alert(reqID);
        if(!DriverJobService.currentJobData){
        DriverJobService.reqOnTheWay(reqID, AuthService.driverEmail, array, index);
        }
        else alert('you have one active job');
        //array.splice(index, 1);
    };
}]);                                                                                   

authController.controller('HelpController', ['$scope','$http','$location', function($scope, $http, $location){
  //$scope.todos = TodosService.todos
    alert("inside help");
}]);
