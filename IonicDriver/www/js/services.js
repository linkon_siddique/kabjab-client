var appService= angular.module('services',['firebase']); 
//appService.constant('FireBaseURL', 'https://driver-track.firebaseio.com/');
appService.factory('AuthService',['$location','$http','$state','$rootScope', function($location, $http, $state, $rootScope){

return{
    driverEmail: '',
    updateDriverEmail: function(email){
        this.driverEmail= email;
        //alert(this.driverEmail);
        $rootScope.$broadcast('email updated');
    
    },
    login: function(urlParam, options){
      var  url= 'http://cirestserver.agilecrafts.com/index.php/api/restacraft/login';
        $http.get(url, {params:{email: urlParam.email, pwd: urlParam.password
        }}).success(function (data){
            //alert("inside success");
            if(!data.error){
                //return true;
                
                options.success();
            }
            else{
                alert('invalid data: '+data.error);
                //return false;
                options.error();
            }
         }).error(function(err) {
            alert('login error: ' + err);
        });
    
    },
    
    hi: function(){
        alert('OMG');
    }

};

}]);


appService.factory('DriverJobService',['$location','$http','$state','$rootScope','$firebaseObject', function($location, $http, $state, $rootScope, $firebaseObject){
var ref = new Firebase("https://driver-track.firebaseio.com/");
var Job={
    jobListUrl: 'http://cirestserver.agilecrafts.com/index.php/api/restacraft/getRequisition',
    fromLocation: null,
    toLocation: null,
    viaLocation: null,
    id: null,
    jobList:[],
    requisitionData: null,
    acceptedData: null,
    currentJobData: null
};
var newLength=0;

    
Job.addToJobList= function(arr){

};    
    
Job.FetchCurrentJobs= function(){

$http.get(this.jobListUrl, {params:{from_location: this.fromLocation, to_location: this.toLocation, via_location: this.viaLocation
        }}).success(function (data){
            //alert("success");
            if(!data.error){
                //this.fromLocation= data[0].from_location;
                //alert("location: "+this.fromLocation); 
               /* for(var i=0; i<data.length; i++){
                    Job.jobList.push(data[i].from_location);
                }*/
                //alert('length: '+Job.jobList.length);
                //$rootScope.$broadcast('user:updated',data);
                newLength= data.length;
                //alert("length fetchJob: "+newLength);
                if($rootScope.$root.$$phase != '$apply' && $rootScope.$root.$$phase != '$digest'){
   $rootScope.$apply(function() {
     Job.requisitionData= data;
   });
 }
                else Job.requisitionData= data;
                
            }
            else{
                alert('invalid data: '+data.error);
            }
         }).error(function(err) {
            alert('fetching joblist error: ' + err);
        });
};

Job.isCurrentListUpdated= function(){
    
$http.get(this.jobListUrl, {params:{from_location: this.fromLocation, to_location: this.toLocation, via_location: this.viaLocation
        }}).success(function (data){
            //alert("success");
            if(!data.error){
               if(data.length==newLength) retun;
                else {
                    newLength=data.length;
                    //alert("is Updated: "+newLength);
                    if($rootScope.$root.$$phase != '$apply' && $rootScope.$root.$$phase != '$digest'){
   $rootScope.$apply(function() {
     Job.requisitionData= data;
   });
 }
                else Job.requisitionData= data;
                     
                    
                return;
                }
            }
            else{
                alert('invalid data: '+data.error);
            }
         }).error(function(err) {
            alert('error: ' + err);
        });
};    
    
Job.pushNewJob= function(){

    this.FetchCurrentJobs();

};
    
Job.checkForNewJob= function(driverEmail){
    //alert('new job check');
    var newJobURL='http://cirestserver.agilecrafts.com/index.php/api/restacraft/getOpenRequisition';
    $http.get(newJobURL, {params:{driveremail: driverEmail
        }}).success(function (data){
            //alert("success");
            if(!data.error){
               if($rootScope.$root.$$phase != '$apply' && $rootScope.$root.$$phase != '$digest'){
   $rootScope.$apply(function() {
        Job.requisitionData= data;
   });
 }
                else Job.requisitionData= data;
                
            }
            else{
                //alert('invalid data: '+data.error);
            }
         }).error(function(err) {
            //alert('Failed: ' + err);
        });
    
        
};

Job.reqAccept= function(reqID, driverEmail, array, index){

var acceptJobURL='http://cirestserver.agilecrafts.com/index.php/api/restacraft/driverAccept/';
    $http.get(acceptJobURL, {params:{requisitionid:reqID, driveremail: driverEmail
        }}).success(function (data){
            //alert("success");
            if(!data.error){
                array.splice(index, 1);
                Job.checkForAcceptedJob(driverEmail);
            }
            else{
                //alert('invalid data: '+data.error);
            }
         }).error(function(err) {
            //alert('error: ' + err);
        });
};    

Job.checkForAcceptedJob= function(driverEmail){
var acceptedJobURL= 'http://cirestserver.agilecrafts.com/index.php/api/restacraft/getAcceptedRequisition';
   
//alert('accepted List');
    $http.get(acceptedJobURL, {params:{driveremail: driverEmail
        }}).success(function (data){
            //alert("success");
            if(!data.error){
                if($rootScope.$root.$$phase != '$apply' && $rootScope.$root.$$phase != '$digest'){
   $rootScope.$apply(function() {
        Job.acceptedData= data;
   });
 }
                else Job.acceptedData= data;
                
                
            }
            else{
                //alert('invalid data: '+data.error);
            }
         }).error(function(err) {
            //alert('error: ' + err);
        });
    
 }; 
    
Job.reqOnTheWay= function(reqID, driverEmail, array, index){
var onTheWayJobURL= 'http://cirestserver.agilecrafts.com/index.php/api/restacraft/driverOntheWay';
   
//alert('accepted List');
    $http.get(onTheWayJobURL, {params:{requisitionid:reqID,driveremail: driverEmail
        }}).success(function (data){
            //alert("success");
            if(!data.error){
                array.splice(index, 1);
                Job.checkForCurrentJob(driverEmail);
            }
            else{
                //alert('invalid data: '+data.error);
            }
         }).error(function(err) {
            //alert('error: ' + err);
        });

};
Job.checkForCurrentJob= function(driverEmail){
var currentJobURL= 'http://cirestserver.agilecrafts.com/index.php/api/restacraft/getCurrentJob';
   
//alert('accepted List');
    $http.get(currentJobURL, {params:{driveremail: driverEmail
        }}).success(function (data){
            //alert("success");
            if(!data.error){
                if($rootScope.$root.$$phase != '$apply' && $rootScope.$root.$$phase != '$digest'){
   $rootScope.$apply(function() {
            //alert(data);
            Job.currentJobData= data;
       
   });
 }
                else Job.currentJobData= data;
            }
            else{
                //alert('invalid data: '+data.error);
            }
         }).error(function(err) {
            //alert('fail: ' + err);
        });
    

};   

Job.driverPosUpdate= function(lat, long){

    ref.update({ latitude: lat, longitude: long });

};
    
Job.updateDriverPosition= function(driverEmail, currLat, currLong){

var updateDriverPosURL= 'http://cirestserver.agilecrafts.com/index.php/api/restacraft/updateDriverPosition';
   
//alert('accepted List');
    $http.get(updateDriverPosURL, {params:{driveremail: driverEmail, currlat: currLat, currlong: currLong
        }}).success(function (data){
            //alert("successfully update driver position");
            if(!data.error){
                if($rootScope.$root.$$phase != '$apply' && $rootScope.$root.$$phase != '$digest'){
   $rootScope.$apply(function() {
            //alert(data);
            
       
   });
 }
                
            }
            else{
                //alert('invalid data: '+data.error);
            }
         }).error(function(err) {
            //alert('fail: ' + err);
        });
    

};    
    
return Job;

}]);


appService.factory('MapService',['$location','$http','$rootScope','$q','DriverJobService',function($location, $http, $rootScope, $q, DriverJobService){

var MapData={
    map: null,
    infoWindow: null,
    marker: null,
    markerArray:[],
    fromLocation: null,
    toLocation: null,
    viaLocation: null,
    fromLatitude: null,
    fromLongitude: null,
    toLatitude: null,
    toLongitude: null,
    viaLatitude: null,
    viaLongitude: null,
    latlng: null,
    geocoder: null,
    currentLatitude: null,
    currentLongitude: null,
    currentLocation: null,
    currentSelectedPlace: null,
    watchID: null
    
    
};


MapData.updateFromLocation= function(value, lat, long){
    
        this.fromLocation= value;
        this.fromLatitude= lat;
        this.fromLongitude= long;
        $rootScope.$broadcast('currLocUpdated');
        //alert(this.fromLocation);
    
    };
    

MapData.updateToLocation= function(value, lat, long){
    
        this.toLocation= value;
        this.toLatitude= lat;
        this.toLongitude= long;
        $rootScope.$broadcast('toLocUpdated');    
        //alert(this.toLocation);
    };
MapData.updateViaLocation= function(value, lat, long){
    
        this.viaLocation= value;
        this.viaLatitude= lat;
        this.viaLongitude=long;
        $rootScope.$broadcast('viaLocUpdated');  
        //alert(this.viaLocation);
    
    };
    
MapData.getMap= function(lat, long) {
    //Creates a new google maps object
        this.latlng = new google.maps.LatLng(lat,long);

};

        
MapData.updateCurrentLat= function(currLat){
    this.currentLatitude= currLat;

};
    
MapData.updateCurrentLong= function(currLong){
    this.currentLongitude= currLong;

};

MapData.watchCurrentPosition= function(){

//var watchPosDefer= $q.defer();
    var options = {timeout:60000};
    if(navigator.geolocation){
        MapData.watchID=  navigator.geolocation.watchPosition(function (position) {
                $rootScope.$apply(function() {
                    DriverJobService.driverPosUpdate(position.coords.latitude, position.coords.longitude);
                    //watchPosDefer.resolve(position);
                });
            }, function (error) {
                $rootScope.$apply(function() {
                    //watchPosDefer.reject(error);
                });
            }, options);
    
       /* MapData.watchID= intel.xdk.geolocation.watchPosition(function(position) {
                $rootScope.$apply(function() {
                DriverJobService.driverPosUpdate(position.coords.latitude, position.coords.longitude);
                    //watchPosDefer.resolve(position);
                });
            }, function(error) {
                $rootScope.$apply(function() {
                    //watchPosDefer.reject(error);
                });
            }, options);*/
        
    }
    else {
        $rootScope.$apply(function() {
                //watchPosDefer.reject(new Error("Geolocation is not supported"));
            });

}

    //return currPositionDeferred.promise;
};
    
MapData.getCurrentLocation= function(){
   // alert('getLocation called');
var currPositionDeferred= $q.defer();
    if(navigator.geolocation){
        //alert('get postion');
         navigator.geolocation.getCurrentPosition(function (position) {
                $rootScope.$apply(function() {
                    currPositionDeferred.resolve(position);
                });
            }, function (error) {
                $rootScope.$apply(function() {
                    MapData.getCurrentLocation();
                    currPositionDeferred.reject(error);
                });
            });
        //alert('navigator found');
      /*  intel.xdk.geolocation.getCurrentPosition(function(position) {
                $rootScope.$apply(function() {
                    currPositionDeferred.resolve(position);
                });
            }, function(error) {
                $rootScope.$apply(function() {
                    currPositionDeferred.reject(error);
                });
            });*/
        }
    else {
        $rootScope.$apply(function() {
                currPositionDeferred.reject(new Error("Geolocation is not supported"));
            });

}
    
    return currPositionDeferred.promise;

};

MapData.convertLatLongToPlace= function(latlong) {

    var latLongConvertDeferred= $q.defer();
    this.geocoder = new google.maps.Geocoder();
    this.geocoder.geocode({'latLng': latlong}, function(results, status) {
      if (status == google.maps.GeocoderStatus.OK) {
      //console.log(results);
        if (results[1]) {
            
         //formatted address
         //alert(results[0].formatted_address)
        //find country name
             for (var i=0; i<results[0].address_components.length; i++) {
            for (var b=0;b<results[0].address_components[i].types.length;b++) {

            //there are different types that might hold a city admin_area_lvl_1 usually does in come cases looking for sublocality type will be more appropriate
                if (results[0].address_components[i].types[b] == "administrative_area_level_1") {
                    //this is the object you are looking for
                    city= results[0].address_components[i];
                    break;
                }
            }
        }
        //city data
            //var cityName=city.short_name;
         //alert(city.short_name + " " + city.long_name);
            $rootScope.$apply(function(){
             MapData.currentLocation = results[0].formatted_address;
            
            });
            latLongConvertDeferred.resolve();
            
           //alert('current loc'+this.currentLocation);
            //document.getElementById('pac-input').value = $scope.currLocation;
            //alert('current location: '+ currLocation);
         //alert($scope.currLocation);
        } else {
          alert("No results found");
        }
      } else {
          alert("Geocoder failed due to: " + status);
          latLongConvertDeferred.reject();
        
      }
         
    });
    return latLongConvertDeferred.promise;
  }; 
    
    
MapData.inputFieldMapSearch= function(inputFieldID){
    
    var inputField = document.getElementById(inputFieldID);
    
        var autocompleteSearch= new google.maps.places.Autocomplete((inputField),
                {
                    types: ['geocode'],
                });
        google.maps.event.addListener(autocompleteSearch, 'place_changed', function() {
            var placeSearch = autocompleteSearch.getPlace();
            if (!placeSearch.geometry) {
                return;
            }
            //document.getElementById('fLat').value = place.geometry.location.lat();
            //document.getElementById('fLong').value = place.geometry.location.lng();

            var addresses = '';
            if (placeSearch.address_components)
            {
                addresses = [
                    (placeSearch.address_components[0] && placeSearch.address_components[0].short_name || ''),
                    (placeSearch.address_components[1] && placeSearch.address_components[1].short_name || ''),
                    (placeSearch.address_components[2] && placeSearch.address_components[2].short_name || '')
                ].join(' ');
            }
        });
    };

MapData.addMarker= function(latlng, markerTitle){
    
    this.marker = new google.maps.Marker({
        position: latlng,
        title: markerTitle,
          
});
// To add the marker to the map, call setMap();
    this.marker.setMap(this.map);
    this.markerArray.push(this.marker);
};  
    
MapData.closeAndResetMarker= function(latlng, markerTitle){
        for (var i = 0, marker; marker = this.markerArray[i]; i++) {
              marker.setMap(null);
            }
        this.markerArray = [];        
        this.addMarker(latlng, markerTitle);
              
    
    };    
    
MapData.addInfoWindow= function(infoWindowContent){

    this.infoWindow = new google.maps.InfoWindow({
        content: infoWindowContent
  });
     
    this.infoWindow.open(this.marker.get('map'), this.marker);

};    
       
MapData.drawMap= function(latlng, mapID, zoomLevel){
  var mapDrawDeferred= $q.defer();
    var mapOptions = {
        center: latlng,
        zoom: zoomLevel,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        zoomControl: true,
        zoomControlOptions: {
            style: google.maps.ZoomControlStyle.SMALL,
            position: google.maps.ControlPosition.LEFT_TOP
        }
    };
    this.map = new google.maps.Map(document.getElementById(mapID), mapOptions);
    var remove_poi = [
    {
      "featureType": "poi",
      "elementType": "labels",
      "stylers": [
        { "visibility": "off" }
      ]
    }
  ];
    this.map.setOptions({styles: remove_poi});   
    mapDrawDeferred.resolve();
    return mapDrawDeferred.promise;
 };

MapData.addMultipleMarker= function(map, locations){
var markerDeferred= $q.defer();
     var latlngbounds = new google.maps.LatLngBounds();
     var latlongArr= [];
     var position= null;      
     var currLocMarker;
     var icon='images/1322492864_Marker.png';
     var infoWindow = new google.maps.InfoWindow();
     var markerIcon, markerAnimation;
     //alert(locations.length);
     for (var i = 0; i < locations.length; i++) {  
         position= new google.maps.LatLng(locations[i][1], locations[i][2]);
         latlongArr.push(position);
         if(i==0){
             markerIcon='';
             markerAnimation='';
         }
         else {
             markerIcon= new google.maps.MarkerImage(icon);
             markerAnimation=google.maps.Animation.BOUNCE;
         }
       currLocMarker = new google.maps.Marker({
    position: position,
    title: locations[i][0],
          map: map,
           icon: markerIcon,
           animation:markerAnimation
        
          
});
         latlngbounds.extend(currLocMarker.position);
         google.maps.event.addListener(currLocMarker, 'click', (function(currLocMarker, i) {
            return function() {
                infoWindow.setContent(locations[i][0]);
                infoWindow.open(map, currLocMarker);
            }
        })(currLocMarker, i));
         
    
     }
     
     map.fitBounds(latlngbounds);
    markerDeferred.resolve(latlongArr);
    return markerDeferred.promise;
      //markers.push(currLocMarker);

};    

MapData.addLineBtwnMarkers= function(map, latlongArr){

//***********ROUTING****************//
 
        //Initialize the Path Array
        var path = new google.maps.MVCArray();
 
        //Initialize the Direction Service
        var service = new google.maps.DirectionsService();
 
        //Set the Path Stroke Color
        var poly = new google.maps.Polyline({ map: map, strokeColor: '#4986E7' });
 for (var i = 0; i < latlongArr.length; i++) {
            if ((i + 1) < latlongArr.length) {
                var src = latlongArr[i];
                var des = latlongArr[i + 1];
                path.push(src);
                poly.setPath(path);
                service.route({
                    origin: src,
                    destination: des,
                    travelMode: google.maps.DirectionsTravelMode.DRIVING
                }, function (result, status) {
                    if (status == google.maps.DirectionsStatus.OK) {
                        for (var i = 0, len = result.routes[0].overview_path.length; i < len; i++) {
                            path.push(result.routes[0].overview_path[i]);
                        }
                    }
                });
            }
        }

};    
    
MapData.convertToPlace= function(latlng){
            var placeSelDeferred= $q.defer();
            this.setPlaceName(latlng,function(converted){
               MapData.currentSelectedPlace= converted;
               MapData.closeMarkerInfoWindow();
               MapData.closeAndResetMarker(latlng, MapData.currentSelectedPlace); 
               MapData.addInfoWindow(MapData.currentSelectedPlace);
               placeSelDeferred.resolve(MapData.currentSelectedPlace);
            //infowindow.close();
           
    //infowindow.setContent('<div><strong>' + converted);
    //infowindow.open(newMarker.get('map'), newMarker);
            });      
    return placeSelDeferred.promise;
    }; 
    
MapData.setPlaceName= function(latlong, callback){
        var placeResult;
        this.geocoder.geocode({'latLng': latlong}, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
      //console.log(results);
                if (results[1]) {
         //formatted address
         //alert(results[0].formatted_address)
        //find country name
           
                for (var i=0; i<results[0].address_components.length; i++) {
                for (var b=0;b<results[0].address_components[i].types.length;b++) {

            //there are different types that might hold a city admin_area_lvl_1 usually does in come cases looking for sublocality type will be more appropriate
                    if (results[0].address_components[i].types[b] == "administrative_area_level_1") {
                    //this is the object you are looking for
                    city= results[0].address_components[i];
                    break;
                }
            }
        }
        placeResult= results[0].formatted_address;            
        MapData.updateCurrentLat(parseFloat(results[0].geometry.location.lat()));
        MapData.updateCurrentLong(parseFloat(results[0].geometry.location.lng()));
            callback(placeResult);
        } else {
          alert("No results found");
        }
      } else {
        alert("Geocoder failed due to: " + status);
      }
         
    });
        
    };
    
MapData.closeMarkerInfoWindow= function(){

    this.infoWindow.close();

};    
    
    
    return MapData;

}]);